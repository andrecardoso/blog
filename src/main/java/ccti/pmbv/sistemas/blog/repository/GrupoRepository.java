package ccti.pmbv.sistemas.blog.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import ccti.pmbv.sistemas.blog.model.Grupo;

public class GrupoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public List<Grupo> todos() {
		return this.manager.createQuery("from Grupo g", Grupo.class).getResultList();
	}
	
	public List<Grupo> porNome(String nome) {
		return this.manager.createQuery("from Grupo where upper(nome) like :nome", Grupo.class)
				.setParameter("nome", nome).getResultList();
	}
	
	public Grupo porId(Long id) {
		return this.manager.find(Grupo.class, id);
	}
	
	public Grupo guardar(Grupo grupo) {
		return this.manager.merge(grupo);
	}
}