package ccti.pmbv.sistemas.blog.service;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Inject;

import ccti.pmbv.sistemas.blog.exception.NegocioException;
import ccti.pmbv.sistemas.blog.model.Noticia;
import ccti.pmbv.sistemas.blog.repository.NoticiaRepository;
import ccti.pmbv.sistemas.blog.util.jpa.Transactional;

public class NoticiaService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private NoticiaRepository noticias;

	@Transactional
	public Noticia salvar(Noticia noticia) throws NegocioException {
		if(noticia.isNovo()) {
	
			noticia.setDataCriacao(new Date());
			noticia.setDataModificacao(new Date());
		}
		
		if(noticia.isExistente()) {
			noticia.setDataModificacao(new Date());
		}
		
		return noticias.guardar(noticia);
	}

}
