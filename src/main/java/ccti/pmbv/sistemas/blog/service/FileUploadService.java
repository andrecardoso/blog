package ccti.pmbv.sistemas.blog.service;

import static java.nio.file.FileSystems.getDefault;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import ccti.pmbv.sistemas.blog.model.Pessoa;
import ccti.pmbv.sistemas.blog.repository.PessoaRepository;
import ccti.pmbv.sistemas.blog.util.cdi.CDIServiceLocator;

public class FileUploadService implements Serializable {

	private static final long serialVersionUID = 1L;

	private Path diretorioRaiz;

	private Path diretorioRaizTemp;

	@PostConstruct
	void init() {
		Path raizAplicacao = getDefault().getPath(System.getProperty("user.home"), ".blog");

		diretorioRaiz = raizAplicacao.resolve("arquivos");
		diretorioRaizTemp = getDefault().getPath(System.getProperty("java.io.tmpdir"), "blog-temp");

		try {
			Files.createDirectories(diretorioRaiz);
			Files.createDirectories(diretorioRaizTemp);
		} catch (IOException e) {
			throw new RuntimeException("Problemas ao tentar criar diretórios.", e);
		}
	}

	public File escrever(String name, byte[] contents) throws IOException {
		File file = new File(diretorioRaizParaArquivos(), name);

		OutputStream out = new FileOutputStream(file);
		out.write(contents);
		out.close();

		return file;
	}

	public static List<File> listar() {
		File dir = diretorioRaizParaArquivos();

		return Arrays.asList(dir.listFiles());
	}

	public static java.io.File diretorioRaizParaArquivos() {
		File dir = new File(diretorioRaiz(), "arquivos-temp");

		if (!dir.exists()) {
			dir.mkdirs();
		}

		return dir;
	}

	public static File diretorioRaiz() {
		// Estamos utilizando um diretório dentro da pasta temporária.
		// No seu projeto, imagino que queira mudar isso para algo como:
		// File dir = new File(System.getProperty("user.home"), ".modulo-pagamento");
		File dir = new File(System.getProperty("java.io.tmpdir"), "blog-temp");

		if (!dir.exists()) {
			dir.mkdirs();
		}

		return dir;
	}

	// Recuperar foto salva no banco de dados
	public byte[] recuperar(String cpf) {

		PessoaRepository pessoas = CDIServiceLocator.getBean(PessoaRepository.class);

		Pessoa pessoa = pessoas.porCpf(cpf);

		byte[] arquivo = pessoa.getFoto();

		return arquivo;
	}
}