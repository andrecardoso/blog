package ccti.pmbv.sistemas.blog.service;

import java.io.Serializable;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import ccti.pmbv.sistemas.blog.exception.NegocioException;
import ccti.pmbv.sistemas.blog.model.Usuario;
import ccti.pmbv.sistemas.blog.repository.UsuarioRepository;
import ccti.pmbv.sistemas.blog.security.MyPasswordEncoder;
import ccti.pmbv.sistemas.blog.util.jpa.Transactional;

public class UsuarioService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private UsuarioRepository usuarios;

	@Transactional
	public Usuario salvar(Usuario usuario) throws NegocioException {
		if (usuario.isNovo()) {
			usuario.setEnabled(Boolean.TRUE);
			// usuario.setDataCriacao(new Date());
						
			if (verificarUsuarioExistente(usuario)) {
				throw new NegocioException("Já existe um usuário com o e-mail informado.");
			}
		}
		
		if (usuario.isExistente() && StringUtils.isBlank(usuario.getSenha())) {
			usuario.setSenha(usuarios.buscarSenha(usuario.getId()));
		}		
		

		usuario.setSenha(MyPasswordEncoder.getPasswordEncoder(usuario.getSenha()));

		return usuarios.guardar(usuario);
	}
	
	public void excluir(Usuario usuario) throws NegocioException {
		if (usuario.isAdmin()) {
			throw new NegocioException("Não pode excluir o administrador!");
		}
		
		usuarios.remover(usuario);
	}
	
	
	public boolean verificarUsuarioExistente(Usuario usuario) {
		Usuario usuarioExistente = usuarios.porEmail(usuario.getEmail());

		if (usuarioExistente != null && usuarioExistente.getEmail().equalsIgnoreCase(usuario.getEmail())) {			
			return true;
		}

		return false; 

	}


	
}
