package ccti.pmbv.sistemas.blog.service;

import java.io.Serializable;

import javax.inject.Inject;

import ccti.pmbv.sistemas.blog.exception.NegocioException;
import ccti.pmbv.sistemas.blog.model.Grupo;
import ccti.pmbv.sistemas.blog.repository.GrupoRepository;
import ccti.pmbv.sistemas.blog.util.jpa.Transactional;

public class GrupoService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private GrupoRepository grupos;

	@Transactional
	public Grupo salvar(Grupo grupo) throws NegocioException {
		
		return grupos.guardar(grupo);
	}
}
