package ccti.pmbv.sistemas.blog.service;

import java.io.Serializable;

import javax.inject.Inject;

import ccti.pmbv.sistemas.blog.exception.NegocioException;
import ccti.pmbv.sistemas.blog.model.Categoria;
import ccti.pmbv.sistemas.blog.repository.CategoriaRepository;
import ccti.pmbv.sistemas.blog.util.jpa.Transactional;

public class CategoriaService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private CategoriaRepository categorias;
	
	@Transactional
	public Categoria salvar(Categoria categoria) throws NegocioException {
		return categorias.guardar(categoria);
	}
	

}
