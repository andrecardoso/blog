package ccti.pmbv.sistemas.blog.model;

import java.io.Serializable;
import java.util.Date;

//@Entity
//@Table(name = "noticias_usuarios", uniqueConstraints = {
//		@UniqueConstraint(columnNames = { "noticia_id", "usuario_id" }) })
public class NoticiasUsuarios implements Serializable {

	private static final long serialVersionUID = 1L;

//	@EmbeddedId
	private NoticiaUsuarioPK id;

//	@NotNull
//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name = "data_modificacao", nullable = false, insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
//	@CreationTimestamp
	private Date dataModificacao;

	public NoticiaUsuarioPK getId() {
		return id;
	}

	public void setId(NoticiaUsuarioPK id) {
		this.id = id;
	}

	public Date getDataModificacao() {
		return dataModificacao;
	}

	public void setDataModificacao(Date dataModificacao) {
		this.dataModificacao = dataModificacao;
	}

}
