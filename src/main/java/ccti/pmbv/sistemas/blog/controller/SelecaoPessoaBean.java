package ccti.pmbv.sistemas.blog.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import ccti.pmbv.sistemas.blog.model.Pessoa;
import ccti.pmbv.sistemas.blog.repository.PessoaRepository;


@Named("selecaoClienteBean")
@ViewScoped
public class SelecaoPessoaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private PessoaRepository pessoas;
	
	private String nome;
	
	private List<Pessoa> pessoasFiltrados;
	
	public void pesquisar() {
		pessoasFiltrados = pessoas.porNome(nome);
	}

	public void selecionar(Pessoa pessoa) {
		RequestContext.getCurrentInstance().closeDialog(pessoa);
	}
	
	public void abrirDialogo() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("modal", false);
		opcoes.put("draggable", false);
		opcoes.put("resizable", false);
		opcoes.put("showHeader", false);
		opcoes.put("responsive", true);
		opcoes.put("minimizable", true);
		opcoes.put("closeOnEscape", true);
		opcoes.put("responsive", true);		
		opcoes.put("fitViewport", true);
		opcoes.put("showEffect", "fade");
		opcoes.put("hideEffect", "fade");
		opcoes.put("minHeight", 300);
		opcoes.put("height", 400);							
		
		RequestContext.getCurrentInstance().openDialog("/dialogos/SelecaoPessoa", opcoes, null);
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Pessoa> getPessoasFiltrados() {
		return pessoasFiltrados;
	}

}