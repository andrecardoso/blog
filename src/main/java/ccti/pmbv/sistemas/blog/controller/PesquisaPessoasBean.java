package ccti.pmbv.sistemas.blog.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import ccti.pmbv.sistemas.blog.exception.NegocioException;
import ccti.pmbv.sistemas.blog.model.Pessoa;
import ccti.pmbv.sistemas.blog.repository.PessoaRepository;
import ccti.pmbv.sistemas.blog.repository.filtros.PessoaFilter;
import ccti.pmbv.sistemas.blog.util.jsf.FacesUtil;


@Named("pesquisaClientesBean")
@ViewScoped
public class PesquisaPessoasBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private PessoaRepository pessoas;
	
	private PessoaFilter filtro;
	private List<Pessoa> pessoasFiltrados;
	
	private Pessoa pessoaSelecionado;
	
	public PesquisaPessoasBean() {
		filtro = new PessoaFilter();
	}
	
	public void pesquisar() {
		pessoasFiltrados = pessoas.filtrados(filtro);
	}
	
	public void excluir() {
		try {
			pessoas.remover(pessoaSelecionado);
			pessoasFiltrados.remove(pessoaSelecionado);
			
			FacesUtil.addInfoMessage("Cliente " + pessoaSelecionado.getNome() + 
					" excluído com sucesso!");
		} catch (NegocioException e) {
			FacesUtil.addErrorMessage(e.getMessage());
		}
	}
	
	public List<Pessoa> getPessoasFiltrados() {
		return pessoasFiltrados;
	}

	public PessoaFilter getFiltro() {
		return filtro;
	}

	public Pessoa getPessoaSelecionado() {
		return pessoaSelecionado;
	}

	public void setPessoaSelecionado(Pessoa pessoaSelecionado) {
		this.pessoaSelecionado = pessoaSelecionado;
	}
	
}
