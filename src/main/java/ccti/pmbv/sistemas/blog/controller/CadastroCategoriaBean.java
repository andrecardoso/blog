package ccti.pmbv.sistemas.blog.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Produces;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import ccti.pmbv.sistemas.blog.exception.NegocioException;
import ccti.pmbv.sistemas.blog.model.Categoria;
import ccti.pmbv.sistemas.blog.repository.CategoriaRepository;
import ccti.pmbv.sistemas.blog.service.CategoriaService;
import ccti.pmbv.sistemas.blog.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroCategoriaBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Produces
	@CategoriaEdicao
	private Categoria categoria;

	private List<Categoria> categoriasRaizes;


	@Inject
	private CategoriaRepository categorias;

	@Inject
	private CategoriaService categoriaService;

	public CadastroCategoriaBean() {
		limpar();
	}

	public void inicializar() {
		if (this.categoria == null) {
			limpar();
		}

		carregarCategorias();

	}

	public void carregarCategorias() {
		categoriasRaizes = categorias.raizes();
	}

	private void limpar() {
		categoria = new Categoria();
		categoriasRaizes = new ArrayList<>();
	}

	public void salvar() {
		try {
			this.categoria = categoriaService.salvar(this.categoria);
			limpar();
			carregarCategorias();

			FacesUtil.addInfoMessage("Categoria salva com sucesso!");
		} catch (NegocioException ne) {
			FacesUtil.addErrorMessage(ne.getMessage());
		}

	}

	public List<Categoria> getCategoriasRaizes() {
		return categoriasRaizes;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public boolean isEditando() {
		return this.categoria.getId() != null;
	}

}
