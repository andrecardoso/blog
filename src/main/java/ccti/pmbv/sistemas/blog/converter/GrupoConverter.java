package ccti.pmbv.sistemas.blog.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import ccti.pmbv.sistemas.blog.model.Grupo;
import ccti.pmbv.sistemas.blog.repository.GrupoRepository;


@FacesConverter(value = "grupoConverter", managed = true)
public class GrupoConverter implements Converter<Object> {

	@Inject
	private GrupoRepository grupos;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Grupo retorno = null;

		if (StringUtils.isNotEmpty(value)) {
			retorno = this.grupos.porId(new Long(value));
		}

		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Grupo grupo = (Grupo) value;
			return grupo == null || grupo.getId() == null ? 
					null : grupo.getId().toString();
		}
		return "";
	}

}