package ccti.pmbv.sistemas.blog.converter;

import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.convert.ClientConverter;

import ccti.pmbv.sistemas.blog.model.Categoria;
import ccti.pmbv.sistemas.blog.repository.CategoriaRepository;

@FacesConverter(forClass = Categoria.class, managed = true)
public class CategoriaConverter implements Converter, ClientConverter {

	@Inject
	private CategoriaRepository categorias;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Categoria retorno = null;

		if (StringUtils.isNotEmpty(value)) {
			Long id = new Long(value);
			retorno = categorias.porId(id);
		}

		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Categoria categoria = (Categoria) value;
			return categoria != null && categoria.getId() != null ? categoria.getId().toString() : null;
		}

		return "";
	}

	@Override
	public Map<String, Object> getMetadata() {

		return null;
	}

	@Override
	public String getConverterId() {
		return "ccti.pmbv.sistemas.blog.Categoria";
	}

}