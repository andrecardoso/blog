package ccti.pmbv.sistemas.blog.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import ccti.pmbv.sistemas.blog.model.Noticia;
import ccti.pmbv.sistemas.blog.repository.NoticiaRepository;

@FacesConverter(forClass = Noticia.class, managed = true)
public class NoticiaConverter implements Converter<Object> {

	@Inject
	private NoticiaRepository noticias;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Noticia retorno = null;

		if (StringUtils.isNotEmpty(value)) {
			Long id = new Long(value);
			retorno = noticias.porId(id);
		}

		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Noticia noticia = (Noticia) value;
			return noticia != null && noticia.getId() != null ? noticia.getId().toString() : null;
		}

		return "";
	}

}