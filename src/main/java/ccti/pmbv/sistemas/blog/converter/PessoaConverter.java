package ccti.pmbv.sistemas.blog.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import ccti.pmbv.sistemas.blog.model.Pessoa;
import ccti.pmbv.sistemas.blog.repository.PessoaRepository;


@FacesConverter(forClass = Pessoa.class, managed = true)
public class PessoaConverter implements Converter<Object> {

	@Inject
	private PessoaRepository pessoas;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Pessoa retorno = null;

		if (StringUtils.isNotEmpty(value)) {
			retorno = this.pessoas.porId(new Long(value));
		}

		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Pessoa pessoa = (Pessoa) value; 
			return pessoa != null && pessoa.getId() != null ? pessoa.getId().toString() : null;
		}
		return "";
	}

}