
insert into usuario (email, nome, senha, enabled) values ('admin@admin', 'admin', '$2a$10$vBuzBdm8Pt2ZDGRFB8EVseCbi2KN70rotnYFnaIorEcY8YrvvEdhO', true);
insert into usuario (email, nome, senha, enabled) values ('vendedor@vendedor', 'vendedor', '$2a$10$946p6oguMjzI6iR/v3juA.HhjE0GU86A.3MnVZTlLU2LXUKYTaauO', true);

insert into grupo (descricao, nome) values ('Administradores', 'ADMINISTRADORES');
insert into grupo (descricao, nome) values ('Vendedores', 'VENDEDORES');
insert into grupo (descricao, nome) values ('Clientes', 'CLIENTES');

insert into usuario_grupo (usuario_id, grupo_id) values (1, 1);
insert into usuario_grupo (usuario_id, grupo_id) values (2, 2);

insert into permissao (descricao, nome) values ('Vendas', 'GERENCIAR_VENDA');
insert into permissao (descricao, nome) values ('Produtos', 'GERENCIAR_PRODUTO');
insert into permissao (descricao, nome) values ('Usuarios', 'GERENCIAR_USUARIO');
insert into permissao (descricao, nome) values ('Grupos', 'GERENCIAR_GRUPO');
insert into permissao (descricao, nome) values ('Clientes', 'GERENCIAR_CLIENTE');
insert into permissao (descricao, nome) values ('Categorias', 'GERENCIAR_CATEGORIA');
insert into permissao (descricao, nome) values ('Registration', 'GERENCIAR_REGISTRATION');

insert into grupo_permissao (grupo_id, permissao_id) values (1, 1);
insert into grupo_permissao (grupo_id, permissao_id) values (1, 2);
insert into grupo_permissao (grupo_id, permissao_id) values (1, 3);
insert into grupo_permissao (grupo_id, permissao_id) values (1, 4);
insert into grupo_permissao (grupo_id, permissao_id) values (1, 5);
insert into grupo_permissao (grupo_id, permissao_id) values (1, 6);
insert into grupo_permissao (grupo_id, permissao_id) values (1, 7);
insert into grupo_permissao (grupo_id, permissao_id) values (2, 1);
insert into grupo_permissao (grupo_id, permissao_id) values (2, 2);
insert into grupo_permissao (grupo_id, permissao_id) values (3, 1);
insert into grupo_permissao (grupo_id, permissao_id) values (3, 7);


insert into categoria (descricao, categoria_pai_id) values ('Categoria 1', null);
insert into categoria (descricao, categoria_pai_id) values ('Categoria 2', null);
insert into categoria (descricao, categoria_pai_id) values ('Categoria 3', null);
insert into categoria (descricao, categoria_pai_id) values ('SubCategoria 1.1', 1);
insert into categoria (descricao, categoria_pai_id) values ('SubCategoria 1.2', 1);
insert into categoria (descricao, categoria_pai_id) values ('SubCategoria 2.1', 2);
insert into categoria (descricao, categoria_pai_id) values ('SubCategoria 3.1', 3);
insert into categoria (descricao, categoria_pai_id) values ('SubCategoria 3.2', 3);
insert into categoria (descricao, categoria_pai_id) values ('SubCategoria 3.3', 3);


